from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class EvalUser(AbstractUser):
    user_role = models.CharField(max_length=32, choices=(('admin', 'admin'), ('dev', 'dev'), ('support', 'support')), default='dev')

    REQUIRED_FIELDS = ['email']


class Bug(models.Model):
    owner = models.ForeignKey('core.EvalUser', related_name='bugs', related_query_name='bug', on_delete=models.SET_DEFAULT, default=1)
    assigned = models.ForeignKey('core.EvalUser', related_name='assigned_bugs', related_query_name='assigned_bug', on_delete=models.SET_DEFAULT, default=1)
    title = models.CharField(max_length=64)
    description = models.TextField()
    state = models.CharField(max_length=16, choices=(('open', 'open'), ('assigned', 'assigned'), ('closed', 'closed')), default='open')
    severity = models.CharField(max_length=16, choices=(('trivial', 'trivial'), ('low', 'low'), ('medium', 'medium'), ('high', 'high'), ('critical', 'critical')), default='open')

    class Meta:
        app_label = 'core'
        verbose_name = "Bug"
        verbose_name_plural = "Bugs"

    def __str__(self):
        return str(self.title)
