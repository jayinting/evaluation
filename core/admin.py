from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *


class EvalUserAdmin(UserAdmin):
    model = EvalUser

    fieldsets = UserAdmin.fieldsets + (
            ('Additional Data', {'fields': ('user_role',)}),
    )

admin.site.register(EvalUser, EvalUserAdmin)
admin.site.register(Bug)
