from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login
from . import auth_decorators as auth_dec
from . import views as core_view


urlpatterns = [
    url(r'^$', core_view.LandingView.as_view(), name='LandingView'),
    # Login
    url(r'^login/$', auth_dec.user_passes_test(lambda u: auth_dec.auth_user_is_anonymous(u), '/bugs/')(login), name='login'),
    #url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'core.views.logout_view', name='logout'),
    # Bugs
    url(r'^bugs/$', login_required(core_view.BugListView.as_view()), name='BugListView'),
    url(r'^bugs/new/$', login_required(core_view.BugCreateView.as_view()), name='BugCreateView'),
    url(r'^bugs/update/(?P<pk>\d+)/$', login_required(core_view.BugUpdateView.as_view()), name='BugUpdateView'),
    # Users
    url(r'^users/$', auth_dec.user_passes_test(lambda u: auth_dec.auth_user_is_admin(u), '/bugs/')(core_view.UserListView.as_view()), name='UserListView'),
    url(r'^users/new/$', auth_dec.user_passes_test(lambda u: auth_dec.auth_user_is_admin(u), '/bugs/')(core_view.UserCreateView.as_view()), name='UserCreateView'),
    url(r'^users/delete/(?P<pk>\d+)/$', auth_dec.user_passes_test(lambda u: auth_dec.auth_user_is_admin(u), '/bugs/')(core_view.UserDeleteView.as_view()), name='UserDeleteView'),
]
