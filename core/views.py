from django.shortcuts import render
from django.contrib.auth import logout
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from . import models as core_mod


# Create your views here.
def logout_view(request):
    logout(request)
    return redirect('/')


class LandingView(TemplateView):
    template_name = "core/index.html"

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            return redirect('core:BugListView')

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class BugListView(ListView):
    model = core_mod.Bug
    template_name = "core/buglist.html"
    context_object_name = "bugs"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(BugListView, self).get_context_data(**kwargs)

        return context


class BugCreateView(CreateView):
    model = core_mod.Bug
    template_name = "core/newbug.html"
    fields = ['owner', 'assigned', 'title', 'description', 'state', 'severity']
    success_url = '/bugs/'

    def get_context_data(self, **kwargs):
        context = super(BugCreateView, self).get_context_data(**kwargs)

        return context


class BugUpdateView(UpdateView):
    model = core_mod.Bug
    template_name = "core/updatebug.html"
    fields = ['owner', 'assigned', 'title', 'description', 'state', 'severity']
    success_url = '/bugs/'

    def get_context_data(self, **kwargs):
        context = super(BugUpdateView, self).get_context_data(**kwargs)

        return context


class UserListView(ListView):
    model = core_mod.EvalUser
    template_name = "core/userlist.html"
    context_object_name = "users"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)

        return context


class UserCreateView(CreateView):
    model = core_mod.EvalUser
    template_name = "core/newuser.html"
    fields = ['username', 'password', 'email', 'user_role']
    success_url = '/users/'

    def get_context_data(self, **kwargs):
        context = super(UserCreateView, self).get_context_data(**kwargs)

        return context

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        self.object.set_password(form.cleaned_data.get('password'))
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


class UserDeleteView(DeleteView):
    model = core_mod.EvalUser
    template_name = "core/deluser.html"
    success_url = '/users/'

    def get_context_data(self, **kwargs):
        context = super(UserDeleteView, self).get_context_data(**kwargs)

        return context
