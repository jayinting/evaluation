from functools import wraps
from django.utils.decorators import available_attrs
from django.utils.encoding import force_str
from django.shortcuts import resolve_url, redirect
from django.core.urlresolvers import resolve


def user_passes_test(test_func, redirect_url='/', redirect_field_name=None):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request):
                return view_func(request, *args, **kwargs)
            path = request.build_absolute_uri()
            # urlparse chokes on lazy objects in Python 3, force to str
            resolved_url = force_str(resolve_url(redirect_url))
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            #from django.contrib.auth.views import redirect_to_login
            #return redirect_to_login(
            #    path, resolved_login_url, redirect_field_name)
            return redirect(resolved_url)
        return _wrapped_view
    return decorator


def auth_user_is_anonymous(request):
    kwargs = resolve(request.path).kwargs
    user = request.user

    if not user.is_authenticated():
        return True
    else:
        return False


def auth_user_is_admin(request):
    kwargs = resolve(request.path).kwargs
    user = request.user

    if user.is_authenticated():
        if user.user_role == 'admin':
            return True
        else:
            return False
    else:
        return False